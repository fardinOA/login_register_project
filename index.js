const express=require('express');
require('dotenv').config();
const mongoose=require('mongoose');
const app=express();

//bodyParser
const bodyParser=require('body-parser');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())

//all routes
const userRouter=require('./routes/user')



//mongo connection
const url=process.env.MONGO_URL
mongoose.connect(url,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useCreateIndex:true
    
})
.then(console.log("server connected"))
.catch(err=>{
    console.log(err);
})

app.use('/user',userRouter);

const port=process.env.PORT || 5000
app.get('/',(req,res)=>{
    res.send(`<h1>I am fardin</h1>`)
})
app.listen(port,console.log("Running on Port ",port));