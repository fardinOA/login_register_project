const User=require('../models/user')
const bcrypt=require('bcrypt');


const registerController= async(req,res)=>{
   try{
       const { name, email, password } = req.body;

       let hasPass = await bcrypt.hash(password, 10);
       let user = new User({
           name,
           email,
           password: hasPass
       });

       let result = await user.save();

       if (result) {
           res.status(201).json({
               message: 'user created successfully',
               User: result
           })
       } else {
           res.json({
               msg: 'can not crate user'
           })
       }
   }
   catch(err){
       res.json({err})
   }
    
}





const loginController = async(req, res) => {
   try{
       const { email, password } = req.body;

       let userInfo = await User.findOne({ email });

       if (userInfo) {
           let comp = await bcrypt.compare(password, userInfo.password)
           if (!comp) {
               res.json({
                   msg: "pass can not match"
               })
           }
           else {
               res.json({
                   message: `Welcome to your account Mr. ${userInfo.name}`
               })
           }


       }
       else {
           res.json({
               msg: 'user not found'
           })
       }
   }
   catch(err){
       res.json({err})
   }
 }   


const allUser= async(req,res)=>{
   try{
       let userInfo = await User.find()
       if (userInfo) {
           res.json({
               message: "All Users",
               userInfo
           })
       }
       else {
           res.json({
               err
           })
       }
   }
   catch(err){
       res.json({
           err
       })
   }
}



const uniqueUser = async(req, res) => {
    try{
        const { email } = req.params;
        const userInfo = await User.findOne({ email })
        if (userInfo) {
            res.json({
                user: userInfo
            })
        }
        else {
            res.json({
                err
            })
        }
    }
    catch(err){
        res.json({
            err
        })
    }
}











module.exports={
    registerController,
    loginController,
    allUser,
    uniqueUser
}