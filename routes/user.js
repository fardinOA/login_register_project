const {registerController, loginController, allUser,uniqueUser}=require('../controller/user')
const express=require('express');
const router=express.Router();

router.post('/register',registerController);
router.post('/login',loginController);
router.get('/all-user',allUser);
router.get('/unique-user/:email',uniqueUser)



module.exports=router;